const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Create Order for Non-Admin User
module.exports.createOrder = async (req, res) => {
    try {
        console.log('Inside createOrder');

        if (req.user.isAdmin) {
            return res.send({ message: "Admins are not allowed to create orders." });
        }

        const userId = req.user.id;
        console.log('userId:', userId);

        const products = req.body.products;
        console.log('products:', products);

        let totalAmount = 0;
        const orderProducts = [];

        for (const product of products) {
            const fetchedProduct = await Product.findById(product.productId);
            if (fetchedProduct) {
                const productInfo = {
                    productId: fetchedProduct._id,
                    productName: fetchedProduct.name,
                    productPrice: fetchedProduct.price,
                    quantity: product.quantity,
                };
                orderProducts.push(productInfo);
                totalAmount += fetchedProduct.price * product.quantity;
            }
        }

        const newOrder = new Order({
            userId,
            products: orderProducts,
            totalAmount,
        });

        await newOrder.save();

        return res.send({ message: 'Order created successfully', order: newOrder });
    } catch (error) {
        console.error("Error creating order:", error);
        return res.status(500).json({ error: 'An error occurred while creating the order' });
    }
};



//--STRETCH GOALS--
//Retrieve authenticated user’s orders
module.exports.getUserOrders = async (req, res) => {
    try {
        const userId = req.user.id;

        const user = await User.findById(userId);
        if (!user) {
            return res.status(404).send("User not found");
        }

        const orders = await Order.find({ userId: userId });

        return res.send(orders);
    } catch (error) {
        console.error("Error retrieving user's orders:", error);
        return res.status(500).send("An error occurred while retrieving user's orders");
    }
};



//Added 9/18
//Delete Order
module.exports.deleteOrder = async (req, res) => {
    try {
        const { orderId } = req.params;

        // Find the order by ID and delete it
        const deletedOrder = await Order.findByIdAndRemove(orderId);

        if (!deletedOrder) {
            return res.status(404).json({ message: 'Order not found' });
        }

        return res.status(200).json({ message: 'Order deleted successfully' });
    } catch (error) {
        console.error('Error:', error);
        return res.status(500).json({ message: 'Internal server error' });
    }
};