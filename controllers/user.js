//controller
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//[SECTION] Check if the email already exists

module.exports.checkEmailExists = (reqBody) => {

    return User.find({email : reqBody.email}).then(result => {

        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
    .catch(err => res.send(err))
};

//User registration
module.exports.registerUser = (reqbody) => {
    let newUser = new User({
        firstName: reqbody.firstName,
        lastName: reqbody.lastName,
        email: reqbody.email,
        mobileNo: reqbody.mobileNo,
        password: bcrypt.hashSync(reqbody.password, 10)
    });

    return newUser.save().then((user, error) => {

            // User registration failed
            if (error) {
                return false;

            // User registration successful
            } else {
                return true;
            }
        })
        .catch(err => err)
    };


//User authentication
module.exports.loginUser = (req, res) => {

	User.findOne({ email: req.body.email }).then(result => {

        console.log(result);

		if(result == null){
			return res.send(false);
		}else{
			
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password)

			if(isPasswordCorrect){
				return res.send({access: auth.createAccessToken(result)})
			}

			else{
				return res.send(false);
			}
		}
	})
	.catch(err=>res.send(err))
}


//Retrieve User Details
module.exports.getProfile = (req, res) => {
    return User.findById(req.user.id)
        .then(result => {
            result.password = "";
            return res.send(result);
        })
        .catch(err => res.send(err))
    };



//--STRETCH GOALS--
//Set user as admin (Admin Only)
module.exports.updateUserAsAdmin = async (req, res) => {
    const userIdToUpdate = req.params.userId;

    try {
        const updatedUser = await User.findByIdAndUpdate(
        userIdToUpdate,
        { isAdmin: true },
        { new: true }
    );

    if (!updatedUser) {
        return res.send({ message: "User not found" });
    }

    res.send({ message: "User updated as admin successfully" });
    } catch (error) {
        console.error(error);
        res.send({ message: "Failed to update user as admin" });
  }
};

