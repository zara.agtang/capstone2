//Route
//Dependencies and Modules
const express = require("express");
const productController = require("../controllers/product");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();

//Create Product (Admin Only)
router.post("/", verify, verifyAdmin, productController.addProduct);

//Retrieve All Products (Admin Only)
router.get("/all", productController.getAllProducts);

//Retrieve All Active Products
router.get("/", productController.getAllActive);

//Retrieve a specific product
router.get("/:productId", productController.getProduct);

//Update product information (Admin Only)
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

//Archive Product (Admin Only)
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);

//Activate Product (Admin Only)
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


//Export Route System
module.exports = router;