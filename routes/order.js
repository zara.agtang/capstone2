// Dependencies
const express = require("express");
const orderController = require("../controllers/order");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

//Routing Component
const router = express.Router();

//Non-admin User Checkout (Create Order)
router.post("/checkout", verify, orderController.createOrder);

//--STRETCH GOALS--
//Retrieve authenticated user’s orders
router.get("/myOrders", verify, orderController.getUserOrders);

//Added 9/18
//User Delete Order
router.delete("/:orderId", verify, orderController.deleteOrder);

//Export Route System
module.exports = router;