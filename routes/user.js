//Route
//Dependencies and Modules
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;


//Routing Component
const router = express.Router();

//[SECTION] Routes - POST
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

//User registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController))
})

//User authentication
router.post("/login", userController.loginUser);

//Retrieve User Details
router.get("/details", verify, userController.getProfile);

//--STRETCH GOALS--
//Set user as admin (Admin Only)
router.put("/updateAdmin/:userId", verify, verifyAdmin, userController.updateUserAsAdmin);



//Export Route System
module.exports = router;